# ================================================
# Non- Linear Data Structure- Heaps
# ================================================

# Other References:
	# Heaps
		# https://www.geeksforgeeks.org/heap-data-structure/?ref=gcse
	# Min Heap
		# https://www.geeksforgeeks.org/min-heap-in-python/
	# Max Heap
		# https://www.geeksforgeeks.org/max-heap-in-python/
	# Heapq and Heapq Operations
		# https://docs.python.org/3/library/heapq.html
	# Heapify Module
		# https://www.educative.io/answers/what-is-the-heapqheapify-module-in-python

# =========
# Dicussion
# =========

# 1. Create an s3 folder and "a_discussion.py"
# Root Folder > s3 > a_discussion.py

    # [SECTION] Min-Heap and Min-Heap Operations

	# Import "heapq" to implement a heap queue
	from heapq import heapify, heappush, heappop

	# Create an empty min_heap list
	min_heap = []
	
	# Use heapify to convert the list into a min-heap
	# Syntax: heapify(target_list)
	heapify(min_heap)

	# Define a function that will print the heap
	def print_min_heap():
        print("Min-heap elements: ")
        for i in min_heap:
            print(i, end = ' ')
        print("\n")
    
    # Next, push an element into an existing heap using the heappush method
    heappush(min_heap, 10)     # min_heap = [10]
    heappush(min_heap, 30)     # min_heap = [10, 30]
    heappush(min_heap, 20)     # min_heap = [10, 30, 20]
    heappush(min_heap, 400)    # min_heap = [10, 30, 20, 400]

    print_min_heap()

    # Result: 10 30 20 400

    # Representation of the heap and its index

    #         [0]       10                    
    #                  /  \                  
    #         [1]    30    20  [2] 
    #               /                  
    #         [3]  400  

    # To print the minimum value of the heap, invoke the zero index element, since it is the root node (or the lowest value of the heapified list)
    print("The head value of heap is "+str(min_heap[0]))

    # Result: 10

    # heapop is function that removes and returns the first element from the min-heap (which has the lowest value)
    element = heappop(min_heap)

    print('The element popped from the min-heap is ' + str(element))

    # Result: 10

    # Call the print_min_heap() function once again to check the elements of the heap
    
    # Result: 20 30 400

    # [SECTION] Max-Heap and Max-Heap Operations

    # Import "heapq" to implement a heap queue
    from heapq import heapify, heappush, heappop

    # Create an empty max_heap list
    max_heap = []

    # Use heapify to convert the list into a min-heap
    heapify(min_heap)

    # Since the heapify function applies the behavior of a min-heap by default, we will by multiply the value by -1, so we can use it as a max-heap
    heappush(max_heap, -1 * 10)     # max_heap = [-10]
    heappush(max_heap, -1 * 30)     # max_heap = [-30, -10]
    heappush(max_heap, -1 * 400)    # max_heap = [-400, -30, -10]
    heappush(max_heap, -1 * 700)    # max_heap = [-700, -400, -30, -10] 
    heappush(max_heap, -1 * 20)     # max_heap = [-700, -400, -30, -10, -20]

    # Representation of the heap and its index


    #         [0]   -700                    
    #               /  \                  
    #         [1]  -400  -30  [2] 
    #              /    \               
    #         [3] -10   -20  [4]

   # To get the element with the highest value, access the lowest element in the list (which is the highest element when it is not multiplied by -1)

   print('Head value of max-heap :' + str(-1 * max_heap[0]))

    # Result: 700

    # Previously, we created a function that prints the elements of a heap. Now we will create function that will iterate and multiply it again by -1, to work with a max-heap
    def print_max_heap():
    print("Max-heap elements: ")
    for i in max_heap:
        print((-1 * i), end=" ")
    print("\n")
 
    print_max_heap()

    # Result: 700 400 30 10 20

    # Since the order of heap is reversed (max to min), heappop removes the element with the lowest value (which is the highest element when it is not multiplied by -1)

    element = heappop(max_heap)

    print('The element popped from the max-heap is ' + str(-1 * element))

    # Result: 700

    # Call the print_max_heap() function once again to check the elements of the heap

    print_max_heap()

    # Result: 400 20 30 10
    
# ========
# Activity
# ========

# Root Folder > activity folder > activity.py